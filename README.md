# SimpleForecast

A simple weather forecast application utilizing the APIs Openweathermap, Ipinfo and Google Places.
Created with Javascript / Jquery, CSS, Bootstrap and the Weather Icons set.

Beta version available at http://inclusivetech.co.uk/simpleforecast_/index.html

## Under the hood
Simpleforecast makes a call to ipinfo.io to compute a user's location from their IP address. If successful, it passes their Lon/Lat coordinates to the openweathermap API which provides present and future weather data for that location, which in turn populates the Simpleforecast UI. 

If the call to ipinfo fails (which could happen for a number of reasons eg. adblockers, too many requests to the server, etc), a default location (New York) is sent to openweathermap and the UI is populated with that data as a fallback.

Additionally Google Places API provides the app with an autocomplete bar which enables the user to search a location which can also send Lon/Lat coordinates to the openweathermap API.

## User Experience
Open APIs such as the openweathermap API provide a huge amount of data, but the key is creating a visualization that enables the user to access the most meaningful information in the most communicative manner possible. I focussed on a minimalist design with large weathercode icons and little text. A few weather details are listed for the current weather, with the next few days represented using only high/low temperatures and icons. 

## Responsiveness / Graceful Degradation
Simpleforecast is arranged using bootstrap grids and designed with a mobile first approach. It scales well across many screen sizes and degrades well if presented with a screen area too big or too small. It has been tested across the last few versions of Safari, Chrome, Firefox, IE as well as common mobile browsers (tested with iOS simulator and Browserstack).

## Tradeoffs 
While there are a few fallbacks for errors, for the OWM server error message I'd like to utilize a nicer system than the present javascript alerts. 

Another issue is that the Weather Icon set isn't a fixed width. As a result certain weather icon codes break the grid on some browsers. In order to minimize this I reduced the font/icon size which unfortunately compromised my intended large-style design. There's also some logic which should be refactored into much more efficient functions (see commented areas in script.js). In spite of this, all these issues are relatively small and could be easily addressed on a future iteration. 
